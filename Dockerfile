FROM phusion/baseimage

ENV PHP_VERSION=7.3
ENV sPHP_VERSION=php7.3

RUN export DEBIAN_FRONTEND=noninteractive && \
    alias minimal_apt_get_install='apt-get install -y --no-install-recommends'

RUN apt-get update && \
    apt-get install -y --no-install-recommends -y supervisor && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN add-apt-repository -y ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        $sPHP_VERSION-fpm \
        $sPHP_VERSION-cli \
        $sPHP_VERSION-mysql \
        $sPHP_VERSION-gd \
        $sPHP_VERSION-json \
        $sPHP_VERSION-mbstring \
        $sPHP_VERSION-opcache \
        $sPHP_VERSION-curl \
        $sPHP_VERSION-xml \
        php-mcrypt \
        php-redis \
        $sPHP_VERSION-zip \
        php-memcached && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    mkdir -p /run/php

RUN     apt-get update && \
        apt-get install -y --no-install-recommends unzip && \
        apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


#RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
#    php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
#    php composer-setup.php --install-dir=/usr/bin --filename=composer && \
#    php -r "unlink('composer-setup.php');"



RUN apt-get update && \
    apt-get install -y --no-install-recommends git && \
    cd /opt && \
    git clone https://github.com/vishnubob/wait-for-it.git && \
    cd wait-for-it && \
    chmod +x wait-for-it.sh && \
    ln -s /opt/wait-for-it/wait-for-it.sh /usr/bin/wait-for-it.sh && \
    apt-get purge -y git && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN apt-get update && apt-get -y install imagemagick && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN apt-get update && \
    apt-get install -y --no-install-recommends -y libfcgi0ldbl && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /etc/service/supervisord
RUN mkdir -p /var/log/supervisor

RUN pwd && ls -lah

COPY ./_build/services/supervisord/run.sh /etc/service/supervisord/run
COPY ./_build/etc/supervisor/supervisord.conf /etc/supervisor/supervisord.conf
RUN touch /etc/service/supervisord/down && \
    chmod +x /etc/service/supervisord/run

COPY ./_build/etc/supervisor/conf.d/php-fpm.conf /etc/supervisor/conf.d/php-fpm.conf

COPY ./docker-healthcheck /usr/local/bin/docker-healthcheck

HEALTHCHECK --interval=1m --timeout=30s CMD docker-healthcheck

WORKDIR /app

EXPOSE 9000

CMD ["/sbin/my_init"]
